const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);
const {User} = require('./users');

const taskSchema = new mongoose.Schema({
    date: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
        maxlength: 50,
        minlength: 3
    },
    description: {
        type: String,
        maxlength: 250,
    },
    timeSpent: {
        type: String,
        required: true,
    },
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    }
});

const Task = new mongoose.model('Task', taskSchema);

function validate(task) {
    const schema = Joi.object({
        date: Joi.string().length(24).required(),
        title: Joi.string().max(50).min(3).required(),
        description: Joi.string().max(250),
        timeSpent: Joi.string().length(11).required(),
        userId: Joi.objectId().required()
        //if userId:Joi.string().required  so valid id or not not checked so installed npm joi-objectid
    });

    return schema.validate({
        date: task.date,
        title: task.title,
        description: task.description,
        timeSpent: task.timeSpent,
        userId:task.userId
    });

};


exports.Task = Task;
exports.validate = validate;