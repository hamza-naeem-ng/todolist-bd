const config = require('config');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

const userSchema = new mongoose.Schema({
    username:{
        type: String,
        required: true,
    },
    email:{
        type: String,
        required: true,
        minlength: 5,
        maxlength:255,
        unique:true
    },
    password:{
        type:String,
        required: true,
        minlength: 8,
        maxlength:1024
    }
});

userSchema.methods.generateAuthToken = function(){
    const token = jwt.sign({_id:this._id,username:this.username},config.get('jwtPrivateKey'));
    return token;
}

const User = new mongoose.model('User',userSchema);

function validate(user){

    const schema = Joi.object({
       username: Joi.string().required(),
       email: Joi.string().min(5).max(255).required().email(),
       password: Joi.string().min(8).max(255).required() 
    });
    return schema.validate({
        username: user.username,
        email:user.email,
        password:user.password
    })
}

exports.User = User;
exports.validate = validate;