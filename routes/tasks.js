const auth = require('../middlewares/auth');
const _ = require('lodash');
const {
    Task,
    validate
} = require('../models/tasks');
const {User} = require('../models/users');
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

router.get('/:date', auth, async (req, res) => {
    
    if(!mongoose.Types.ObjectId.isValid(req.query.userId))
    return res.status(400).send("Invalid User Id sent");

    if (req.query.list=="true") {
        let taskList = [];
        const today = new Date(req.params.date);

        for (let i = 0; i < 7; i++) {
            const _day = new Date(today);
            _day.setDate(today.getDate() + i)
            let tasks = await Task.find({
                date: _day.toISOString(),
                userId: req.query.userId
            }).sort('name');
            taskList.push(tasks);
        }
        res.send(taskList);
        
    } else {

        const tasks = await Task.find({
            date: req.params.date,
            userId: req.query.userId
        }).sort('name');
        res.send(tasks);
    }
});

router.post('/', auth, async (req, res) => {

    const {
        error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const user = await User.findById(req.body.userId);
    if(!user) res.status(400).send("Invalid User Id");

    let task = new Task(_.pick(req.body, ['date', 'title', 'description', 'timeSpent','userId']));

    task = await task.save();
    res.send(task);

});

router.put('/:id', auth, async (req, res) => {
    const {
        error
    } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const user = await User.findById(req.body.userId);
    if(!user) res.status(400).send("Invalid User Id");

    const task = await Task.findByIdAndUpdate(req.params.id,
        _.pick(req.body, ['date', 'title', 'description', 'timeSpent','userId']), {
            new: true
        });

    if (!task) return res.status(404).send("No such Task exist");

    res.send(task);

})

router.delete('/:id', auth, async (req, res) => {

    const task = await Task.findByIdAndRemove(req.params.id);

    if (!task) return res.status(404).send("No such Task exist");

    res.send(task);
})

module.exports = router;