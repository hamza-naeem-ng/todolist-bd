const config = require('config');
const express = require("express");
var cors = require('cors');
const mongoose =require('mongoose');
const Joi = require('@hapi/joi');
const taskRH = require('./routes/tasks');
const homeRH = require('./routes/home');
const userRH = require('./routes/users');
const authRH = require('./routes/auth');

if(!config.get('jwtSecureKey')){
    console.error("FATAL ERROR: jwtSecureKey is not defined");
    process.exit(1);
}

mongoose.connect(config.get('db')
, { useNewUrlParser: true , useUnifiedTopology: true, useCreateIndex: true,useFindAndModify:false })
.then(()=>{console.log('connected to mongodb')})
.catch((err)=>{console.log(JSON.stringify(err))});


const app = express();

app.use(express.json());
app.use(cors({
    exposedHeaders: ['x-auth-token'],
  }));
app.use('/api/tasks',taskRH);
app.use('/api/users',userRH);
app.use('/api/auth',authRH);
app.use('/',homeRH);
require('./prod')(app);


const port = process.env.PORT || 7000; 

app.listen(port, () => {
    console.log(`Server is ready to listen on port ${port}`)
    });
  
    console.log(process.env.NODE_ENV);